//
//  CodeViewController.h
//  Book Price
//
//  Created by Valiantsin Vasiliavitski on 6/11/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CodeViewController : UIViewController

@property(nonatomic, strong) NSDictionary* dic;

@end
