//
//  URLViewController.m
//  Book Price
//
//  Created by Valiantsin Vasiliavitski on 6/20/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "URLViewController.h"
#import "ViewController.h"
@import Firebase;

@interface URLViewController () <NSURLSessionDataDelegate, NSURLSessionDelegate>
@property (weak, nonatomic) IBOutlet UITextField *url;
@property (strong, nonatomic) FIRDatabaseReference* ref;

@property (nonatomic, strong) NSUserDefaults *defaults;

@end

@implementation URLViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.defaults = [NSUserDefaults standardUserDefaults];
    if ([self.defaults objectForKey:@"URL"]) {
        
        self.url.text = [self.defaults objectForKey:@"URL"];
    }
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecogized)];
    [self.view addGestureRecognizer:recognizer];

    // Do any additional setup after loading the view.
}

-(void)tapRecogized {
    [self.view endEditing:YES];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)connect:(id)sender {
    NSURL *url = [NSURL URLWithString:self.url.text];
//    if([self connectedToInternet] == NO){
//        // Not connected to the internet
//        dispatch_async(dispatch_get_main_queue(), ^{
//            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Нет соединения с интернетом" message:@"Проверьте подключение" preferredStyle:UIAlertControllerStyleAlert];
//            [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
//            [self presentViewController:alert animated:YES completion:nil];
//        });
//    }
//    else{
//        // Connected to the internet
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://google.com"]];
    request.HTTPMethod = @"HEAD";
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
    NSURLSessionTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (!error && response != nil){
            
//            if ([[UIApplication sharedApplication] canOpenURL:url]) {
            
                NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
                request.HTTPMethod = @"HEAD";
                NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
                NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:nil];
                NSURLSessionTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                    if (!error){
                        if (((NSHTTPURLResponse*)response).allHeaderFields != nil) {
                            [self.defaults setObject:[url absoluteString] forKey:@"URL"];
                            
                            FIROptions *options = [FIROptions defaultOptions];
                            options.databaseURL = [self.defaults objectForKey:@"URL"];
                            [FIRApp configureWithOptions:options];
                            @try{
                                self.ref = [[FIRDatabase database] reference];
                                ViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"barcodeReader"];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    controller.ref = self.ref;
                                    [self presentViewController:controller animated:YES completion:nil];
                                });
                                
                            }
                            @catch (NSException *e) {
                                [[FIRApp defaultApp] deleteApp:^(BOOL success) {
                                    NSLog(@"%@",e);
                                }];
                                [self.defaults setObject:nil forKey:@"URL"];
                                [self showAlert];
                            }
                            
                        }
                    } else {
                        [self.defaults setObject:nil forKey:@"URL"];
                        [self showAlert];
                    }
                }];
                [task resume];
//            } else {
//                [self.defaults setObject:nil forKey:@"URL"];
//                [self showAlert];
//            }
            
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Нет подключения к интернету" message:@"Проверьте подключение" preferredStyle:UIAlertControllerStyleAlert];
                [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
                [self presentViewController:alert animated:YES completion:nil];
            });
        }
        
    }];
    [task resume];
        
//    }
}

//- (BOOL)connectedToInternet
//{
//    NSString *urlString = @"http://www.google.com/";
//    NSURL *url = [NSURL URLWithString:urlString];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    [request setHTTPMethod:@"HEAD"];
//    NSHTTPURLResponse *response;
//
//    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error: NULL];
//
//
//    return ([response statusCode] == 200) ? YES : NO;
//}

-(void) showAlert {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Неверный URL" message:@"Повторите ввод" preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    });
    
}

- (IBAction)exit:(id)sender {
    
    exit(0);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
