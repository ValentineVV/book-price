///Users/valiantsin_vasiliavi/Desktop/Book Price/Book Price/Book Price
//  CodeViewController.m
//  Book Price
//
//  Created by Valiantsin Vasiliavitski on 6/11/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "CodeViewController.h"

@interface CodeViewController ()
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *decrip;
@property (weak, nonatomic) IBOutlet UILabel *cost;

@property (weak, nonatomic) IBOutlet UIButton *back;


@end

@implementation CodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.name.text = [self.name.text stringByAppendingString:[NSString stringWithFormat:@" %@",[self.dic objectForKey:@"name"]]];
    self.decrip.text = [self.decrip.text stringByAppendingString:[NSString stringWithFormat:@" %@",[self.dic objectForKey:@"description"]]];
    self.cost.text = [self.cost.text stringByAppendingString:[NSString stringWithFormat:@" %@ руб.",[self.dic objectForKey:@"price"]]];
    
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: [self.dic objectForKey:@"photoUrl"]]];
        if ( data == nil ) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [[self.name.bottomAnchor constraintEqualToAnchor:self.decrip.topAnchor constant:-20] setActive:YES];[[self.name.bottomAnchor constraintEqualToAnchor:self.decrip.topAnchor constant:-20] setActive:YES];
            });
            return;
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.img.image = [UIImage imageWithData: data];
                UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageWithData:data]];
                [self.view addSubview:imgView];
                
                [[imgView.leadingAnchor constraintEqualToAnchor:self.name.leadingAnchor] setActive:YES];
                [[imgView.trailingAnchor constraintEqualToAnchor:self.name.trailingAnchor] setActive:YES];
                [[imgView.topAnchor constraintEqualToAnchor:self.name.bottomAnchor constant:20] setActive:YES];
                [[imgView.bottomAnchor constraintEqualToAnchor:self.decrip.topAnchor constant:-20] setActive:YES];
                imgView.translatesAutoresizingMaskIntoConstraints = NO;
                [imgView addConstraint:[NSLayoutConstraint constraintWithItem:imgView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:imgView attribute:NSLayoutAttributeWidth multiplier:(imgView.frame.size.height / imgView.frame.size.width) constant:0]];
            });
        }
    });
    
    
    // Do any additional setup after loading the view.
}
- (IBAction)back:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
