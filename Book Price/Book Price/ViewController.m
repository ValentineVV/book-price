//
//  ViewController.m
//  Book Price
//
//  Created by Valiantsin Vasiliavitski on 6/11/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "CodeViewController.h"


@interface ViewController () <AVCaptureMetadataOutputObjectsDelegate>

@property (nonatomic, strong) NSMutableString* mCode;
@property (nonatomic, strong) AVCaptureSession* mCaptureSession;
@property (nonatomic, strong) NSDictionary *dictionary;
@property (nonatomic, strong) CodeViewController* controller;
@property (nonatomic, assign) Boolean flag;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([FIRApp defaultApp] == nil){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        FIROptions *options = [FIROptions defaultOptions];
        options.databaseURL = [defaults objectForKey:@"URL"];
        [FIRApp configureWithOptions:options];
        [FIRDatabase database].persistenceEnabled = YES;
        self.ref = [[FIRDatabase database] reference];
    }
    
    
    self.flag = YES;
    
    [[self.ref child:@"products"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
        if ([snapshot.value isKindOfClass:[NSDictionary class]]){
            self.dictionary = snapshot.value;
        }
    }];
    
    self.mCaptureSession = [[AVCaptureSession alloc] init];
    
    AVCaptureDevice *videoCaptureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;
    
    AVCaptureDeviceInput *videoInput = [AVCaptureDeviceInput deviceInputWithDevice:videoCaptureDevice error:&error];
    
    if ([self.mCaptureSession canAddInput:videoInput]) {
        [self.mCaptureSession addInput:videoInput];
        
        
        AVCaptureMetadataOutput *metadataOutput = [[AVCaptureMetadataOutput alloc] init];
        
        if ([self.mCaptureSession canAddOutput:metadataOutput]) {
            [self.mCaptureSession addOutput:metadataOutput];
            
            [metadataOutput setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
            [metadataOutput setMetadataObjectTypes:@[AVMetadataObjectTypeQRCode, AVMetadataObjectTypeEAN13Code]];
        } else {
            NSLog(@"Could not add metadata output.");
        }
        
        AVCaptureVideoPreviewLayer *previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.mCaptureSession];
        previewLayer.frame = self.view.layer.bounds;
        [self.view.layer addSublayer:previewLayer];
        
//        add rectangle sublayer
        UIBezierPath *rectangle = [UIBezierPath bezierPathWithRect:CGRectMake(0, [UIScreen mainScreen].bounds.size.height/2 - 30, [UIScreen mainScreen].bounds.size.width, 60)];
        [[UIColor clearColor] setFill];
        [rectangle fill];
        [[UIColor redColor] setStroke];
        rectangle.lineWidth = 1;
        [rectangle stroke];
        
        CAShapeLayer *boundingLayer = [[CAShapeLayer alloc] init];
        boundingLayer.path = rectangle.CGPath;
        boundingLayer.fillColor = [UIColor clearColor].CGColor;
        boundingLayer.strokeColor = [UIColor greenColor].CGColor;
        [self.view.layer addSublayer:boundingLayer];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - ([UIScreen mainScreen].bounds.size.width/2 + 90)/2, [UIScreen mainScreen].bounds.size.height/2 - 90, [UIScreen mainScreen].bounds.size.width/2 + 90, 60)];
        label.text = @"Сканируйте штрихкод";
        label.font = [UIFont systemFontOfSize:28];
        label.textColor = [UIColor greenColor];
        [self.view addSubview:label];
        
        UIButton *backButton = [UIButton buttonWithType:UIButtonTypeSystem];
        [backButton setTitle:@"Выйти" forState:UIControlStateNormal];
        [self.view addSubview:backButton];
        backButton.titleLabel.font = [UIFont systemFontOfSize:24];
        [[backButton.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor constant:-90] setActive:YES];
        [[backButton.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor] setActive:YES];
        backButton.translatesAutoresizingMaskIntoConstraints = NO;
        [backButton addTarget:self action:@selector(backButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        backButton.tintColor = [UIColor redColor];
        
        
        
        [self.mCaptureSession startRunning];
    } else {
        NSLog(@"Could not add video input: %@", [error localizedDescription]);
    }

}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

-(void)backButtonPressed {
    
    exit(0);
}

- (void)captureOutput:(AVCaptureOutput *)output didOutputMetadataObjects:(NSArray<__kindof AVMetadataObject *> *)metadataObjects fromConnection:(AVCaptureConnection *)connection {
    
    if (self.mCode == nil) {
        self.mCode = [[NSMutableString alloc] initWithString:@""];
    }

    [self.mCode setString:@""];

    for (AVMetadataObject *metadataObject in metadataObjects) {
        AVMetadataMachineReadableCodeObject *readableObject = (AVMetadataMachineReadableCodeObject *)metadataObject;
        if ([metadataObject.type isEqualToString:AVMetadataObjectTypeEAN13Code]) {
            [self.mCode appendFormat:@"%@", readableObject.stringValue];
        }
    }

    NSDictionary *dic = [self.dictionary objectForKey:self.mCode];
    if (dic != nil) {
        self.controller = [self.storyboard instantiateViewControllerWithIdentifier:@"codeViewController"];
        self.controller.dic = dic;
        [self presentViewController:self.controller animated:YES completion:nil];
    } else {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Ошибка" message:[NSString stringWithFormat:@"Штрих код %@ не найден базе данных.", self.mCode] preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
        self.controller = [self.storyboard instantiateViewControllerWithIdentifier:@"codeViewController"];
        self.controller.dic = [self.dictionary objectForKey:self.mCode];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if ([self.mCaptureSession isRunning] == NO) {
        [self.mCaptureSession startRunning];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        if (!granted) {
            if (self.flag) {
                NSString *mediaType = AVMediaTypeVideo;
                AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:mediaType];
                if(authStatus == AVAuthorizationStatusAuthorized) {
                    // do your logic
                } else if(authStatus == AVAuthorizationStatusDenied){
                    // denied
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Нет доступа к камере" message:@"Пожалуйста, дайте доступ к камере в настройках" preferredStyle:UIAlertControllerStyleAlert];
                    [alert addAction:[UIAlertAction actionWithTitle:@"Настройки" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
                    } ]];
                    [self presentViewController:alert animated:YES completion:nil];
                    return;
                    
                } else if(authStatus == AVAuthorizationStatusRestricted){
                    // restricted, normally won't happen
                }
            }
            self.flag = NO;
        }
    }];
}

-(void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    
    if ([self.mCaptureSession isRunning]) {
        [self.mCaptureSession stopRunning];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
