//
//  ContainerViewController.m
//  Book Price
//
//  Created by Valiantsin Vasiliavitski on 6/20/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "ContainerViewController.h"

@interface ContainerViewController ()

@end

@implementation ContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"URL"]) {
        UIViewController *contr = [self.storyboard instantiateViewControllerWithIdentifier:@"barcodeReader"];
        [self addChildViewController:contr];
        [self.view addSubview:contr.view];
    } else {
        UIViewController *contr = [self.storyboard instantiateViewControllerWithIdentifier:@"URLView"];
        [self addChildViewController:contr];
        [self.view addSubview:contr.view];
    }
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
