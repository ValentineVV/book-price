//
//  ViewController.h
//  Book Price
//
//  Created by Valiantsin Vasiliavitski on 6/11/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <UIKit/UIKit.h>
@import Firebase;

@interface ViewController : UIViewController

//@property (nonatomic, strong) NSUserDefaults *defaults;
@property (strong, nonatomic) FIRDatabaseReference* ref;

@end

